/* Stratogem
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "weapon.h"

Weapon::Weapon(Context* context): Component(context)
{
}

void Weapon::OnSetEnabled() { Component::OnSetEnabled(); }
void Weapon::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->Yaw(Random(-180.f, 180.f));

    StaticModel* weaponModel{ node_->CreateComponent<StaticModel>() };
    weaponModel->SetCastShadows(true);

    const StringVector weaponNames{ "Cannon", "Missiles", "Gatling" };
    const String modelName{ weaponNames.At(Random(static_cast<int>(weaponNames.Size()))) };
    weaponModel->SetModel(RES(Model, "Models/Hardware/" + modelName + ".mdl"));
}

void Weapon::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Weapon::OnMarkedDirty(Node* node) {}
void Weapon::OnNodeSetEnabled(Node* node) {}
bool Weapon::Save(Serializer& dest) const { return Component::Save(dest); }
bool Weapon::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Weapon::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void Weapon::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Weapon::GetDependencyNodes(PODVector<Node*>& dest) {}
void Weapon::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
