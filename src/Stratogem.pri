HEADERS += \
    $$PWD/locomotion.h \
    $$PWD/luckey.h \
    $$PWD/mastercontrol.h \
    $$PWD/sceneobject.h \
    $$PWD/spawnmaster.h \
    $$PWD/vehicle.h \
    $$PWD/weapon.h

SOURCES += \
    $$PWD/locomotion.cpp \
    $$PWD/mastercontrol.cpp \
    $$PWD/sceneobject.cpp \
    $$PWD/spawnmaster.cpp \
    $$PWD/vehicle.cpp \
    $$PWD/weapon.cpp
