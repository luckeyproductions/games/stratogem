/* Stratogem
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef LOCOMOTION_H
#define LOCOMOTION_H

#include "mastercontrol.h"

enum LocomotionType{ LOCO_NONE = -1, LOCO_WALK, LOCO_BUGGY, LOCO_TANK, LOCO_FLY, LOCO_ALL };
static const StringVector locomotionNames{ "Walker", "Buggy", "Tank", "Flyer" };

class Locomotion: public Component
{
    DRY_OBJECT(Locomotion, Component);

public:
    Locomotion(Context* context);
    void OnSetEnabled() override;

    void SetLocomotionType(LocomotionType type);
    LocomotionType GetLocomotionType() const { return locomotionType_; }
    Vector3 GetLocomotionOffset() const;

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

private:
    StaticModel* locomotionModel_;
    LocomotionType locomotionType_;
};

#endif // LOCOMOTION_H
