/* Stratogem
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "locomotion.h"

Locomotion::Locomotion(Context* context): Component(context),
    locomotionModel_{ nullptr },
    locomotionType_{ LOCO_NONE }
{
}

void Locomotion::OnNodeSet(Node* node)
{
    if (!node)
        return;

    locomotionModel_ = node_->CreateComponent<StaticModel>();
    locomotionModel_->SetCastShadows(true);

    SetLocomotionType(static_cast<LocomotionType>(Random(LOCO_ALL)));
}

void Locomotion::SetLocomotionType(LocomotionType type)
{
    if (locomotionType_ == type)
        return;

    locomotionType_ = type;

    if (locomotionType_ == LOCO_NONE)
    {
        locomotionModel_->SetModel(nullptr);
    }
    else
    {
        const String modelName{ locomotionNames.At(locomotionType_) };
        locomotionModel_->SetModel(RES(Model, "Models/Hardware/" + modelName + ".mdl"));
    }

    RigidBody* rb{ node_->GetComponent<RigidBody>() };
    if (locomotionType_ == LOCO_WALK)
    {
        rb->SetAngularFactor(Vector3::UP);

        Quaternion straighten{};
        straighten.FromRotationTo(node_->GetWorldUp(), Vector3::UP);
        node_->Rotate(straighten);
    }
    else
    {
        rb->SetAngularFactor(Vector3::ONE);
    }

    CollisionShape* collider{ node_->GetComponent<CollisionShape>() };
    const Vector3 offset{ GetLocomotionOffset() };
    collider->SetBox(offset + Vector3{ 4.f, 0.f, 5.f } / Sqrt(offset.y_), -.333f * offset );
}

Vector3 Locomotion::GetLocomotionOffset() const
{
    float offsetY{};

    switch (locomotionType_) {
    default: case LOCO_NONE: offsetY = .125f; break;
    case LOCO_WALK:  offsetY = 4.f; break;
    case LOCO_BUGGY: offsetY = 1.5f; break;
    case LOCO_TANK:  offsetY = 1.25f; break;
    case LOCO_FLY:   offsetY = .5f; break;
    }

    return Vector3::UP * offsetY;
}

void Locomotion::OnSetEnabled() { Component::OnSetEnabled(); }
void Locomotion::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Locomotion::OnMarkedDirty(Node* node) {}
void Locomotion::OnNodeSetEnabled(Node* node) {}
bool Locomotion::Save(Serializer& dest) const { return Component::Save(dest); }
bool Locomotion::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Locomotion::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void Locomotion::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Locomotion::GetDependencyNodes(PODVector<Node*>& dest) {}
void Locomotion::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
