/* Stratogem
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "weapon.h"
#include "locomotion.h"

#include "vehicle.h"

void Vehicle::RegisterObject(Context* context)
{
    context->RegisterFactory<Vehicle>();
    context->RegisterFactory<Weapon>();
    context->RegisterFactory<Locomotion>();
}

Vehicle::Vehicle(Context* context): SceneObject(context),
    weapon_{ nullptr },
    locomotion_{ nullptr }
{
}

void Vehicle::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->Yaw(Random(360));

    RigidBody* rb{ node_->CreateComponent<RigidBody>() };
    rb->SetMass(1.f);
    CollisionShape* collider{ node_->CreateComponent<CollisionShape>() };
    collider->SetBox({ 4.f, 1.f, 5.f });

    StaticModel* interface{ node_->CreateComponent<StaticModel>() };
    interface->SetCastShadows(true);
    interface->SetModel(RES(Model, "Models/Hardware/Interface.mdl"));

    weapon_ = node_->CreateChild("Gun")->CreateComponent<Weapon>();
    locomotion_ = node_->CreateComponent<Locomotion>();

    PODVector<StaticModel*> models{};
    node_->GetComponents<StaticModel>(models, true);
    for (StaticModel* model: models)
    {
        model->SetMaterial(0, RES(Material, "Materials/Paint.xml"));
        model->SetMaterial(1, RES(Material, "Materials/Metal.xml"));
        model->SetMaterial(2, RES(Material, "Materials/Chrome.xml"));
    }
}

void Vehicle::Set(const Vector3& position)
{
    Vector3 grounded{ position };

    PhysicsWorld* physicsWorld{ GetScene()->GetComponent<PhysicsWorld>() };
    PhysicsRaycastResult result{};
    const Ray ray{ position + Vector3::UP * 500.f, Vector3::DOWN };
    physicsWorld->RaycastSingle(result, ray, 1000.f);

    if (result.distance_ != M_INFINITY)
    {
        grounded = result.position_;

        if (locomotion_->GetLocomotionType() != LOCO_WALK)
        {
            Quaternion rot{};
            rot.FromRotationTo(node_->GetWorldUp(), result.normal_);
            node_->Rotate(rot, TS_WORLD);
        }
    }

    SceneObject::Set(grounded);

    node_->Translate(locomotion_->GetLocomotionOffset() + Vector3::UP * 2/3.f);
}

void Vehicle::OnSetEnabled() { LogicComponent::OnSetEnabled(); }
void Vehicle::Start() {}
void Vehicle::DelayedStart() {}
void Vehicle::Stop() {}
void Vehicle::Update(float timeStep) {}
void Vehicle::PostUpdate(float timeStep) {}
void Vehicle::FixedUpdate(float timeStep) {}
void Vehicle::FixedPostUpdate(float timeStep) {}
void Vehicle::OnSceneSet(Scene* scene) { LogicComponent::OnSceneSet(scene); }
void Vehicle::OnMarkedDirty(Node* node) {}
void Vehicle::OnNodeSetEnabled(Node* node) {}
bool Vehicle::Save(Serializer& dest) const { return LogicComponent::Save(dest); }
bool Vehicle::SaveXML(XMLElement& dest) const { return LogicComponent::SaveXML(dest); }
bool Vehicle::SaveJSON(JSONValue& dest) const { return LogicComponent::SaveJSON(dest); }
void Vehicle::MarkNetworkUpdate() { LogicComponent::MarkNetworkUpdate(); }
void Vehicle::GetDependencyNodes(PODVector<Node*>& dest) {}
void Vehicle::DrawDebugGeometry(DebugRenderer* debug, bool depthTest)
{
    GetComponent<CollisionShape>()->DrawDebugGeometry(debug, depthTest);
}
