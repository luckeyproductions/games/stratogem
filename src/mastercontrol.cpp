/* Stratogem
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "spawnmaster.h"
#include "vehicle.h"


#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context* context): Application(context),
    scene_{ nullptr }
{
}

void MasterControl::Setup()
{
    SetRandomSeed(GetSubsystem<Time>()->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "Stratogem.log";
    engineParameters_[EP_WINDOW_TITLE] = "Stratogem";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Resources;";
}

void MasterControl::Start()
{
    context_->RegisterSubsystem(this);
    context_->RegisterSubsystem<SpawnMaster>();
    Vehicle::RegisterObject(context_);

    CreateScene();

//    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(MasterControl, RenderDebug));
}

void MasterControl::Stop()
{
    engine_->DumpResources(true);
}

void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<PhysicsWorld>();
    scene_->CreateComponent<DebugRenderer>();

    Renderer* renderer{ GetSubsystem<Renderer>() };
    Zone* defaultZone{ renderer->GetDefaultZone() };
    defaultZone->SetAmbientColor(Color::WHITE.Lerp(Color::CYAN, .23f).Lerp(Color::BLACK, .55f));

    RenderPath* defaultRenderPath{ renderer->GetDefaultRenderPath() };
    defaultRenderPath->Append(RES(XMLFile, "PostProcess/FXAA3.xml"));

    //Light
    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition({ 2.0f, 3.0f, 1.0f });
    lightNode->LookAt(Vector3::DOWN * 5.f);
    Light* light{ lightNode->CreateComponent<Light>() };
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetCastShadows(true);
    light->SetShadowIntensity(.42f);
    light->SetShadowCascade({ 10.f, 20.f, 40.f, 80.f, .75f });

    //Camera
    Node* cameraNode{ scene_->CreateChild("Camera") };
    cameraNode->SetPosition(Vector3::ONE * 17.f);
    cameraNode->LookAt(Vector3::UP * 5.f);
    Camera* camera{ cameraNode->CreateComponent<Camera>() };
    camera->SetFov(80.f);
    GetSubsystem<Renderer>()->SetViewport(0, new Viewport(context_, scene_, camera));

    Node* terrainNode{ scene_->CreateChild("Floor") };
    Terrain* terrain{ terrainNode->CreateComponent<Terrain>() };
//    floor->SetModel(RES(Model, "Models/Box.mdl"));
    terrain->SetSpacing({ 1.f, .125f, 1.f });
    terrain->SetMaterial(RES(Material, "Materials/GrassRockTiled.xml"));
    terrain->SetHeightMap(RES(Image, "Textures/Heightmap.png"));

    terrainNode->Translate(Vector3::DOWN * 8.f);
    RigidBody* terrainRb{ terrainNode->CreateComponent<RigidBody>() };
    terrainRb->SetFriction(.8f);
    CollisionShape* collider{ terrainNode->CreateComponent<CollisionShape>() };
    collider->SetTerrain();

    //Vehicles
    SpawnMaster* spawn{ GetSubsystem<SpawnMaster>() };
    const int r{ 8 };
    const int c{ 8 };
    for (int v{ 0 }; v < r * c; ++v)
    {
        Vehicle* vehicle{ spawn->Create<Vehicle>() };
        vehicle->Set({ 9.f * ((v % r) - r / 3), 0.f, 9.f * ((v / r) - r / 3 ) });
    }

    // Music

    Scene* musicScene{ new Scene(context_) };
    Node* musicNode{ musicScene->CreateChild("Music") };
    SoundSource* musicSource = musicNode->CreateComponent<SoundSource>();
    musicSource->SetSoundType(SOUND_MUSIC);

    Sound* music{ RES(Sound, "Music/GreatOwl - Yai.ogg") };
    music->SetLooped(true);

    musicSource->Play(music);
    musicSource->SetGain(0.23f);
}

void MasterControl::RenderDebug(StringHash eventType, VariantMap& eventData)
{
    SpawnMaster* spawn{ GetSubsystem<SpawnMaster>() };

    for (Vehicle* v: spawn->GetActive<Vehicle>())
    {
        v->DrawDebugGeometry(scene_->GetComponent<DebugRenderer>(), true);
    }
}

